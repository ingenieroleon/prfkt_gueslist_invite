var prkt_interval_email_send = 5000;

jQuery(window).ready(function () {

    jQuery("#prkt-guest-list-content-form form").submit(function (e) {

        var data = jQuery(this).serializeArray();
        data.action = "prkt_gl_ajax_post_submit_form";
        data.pop();
        data.push({name: 'mail', value: jQuery(this).find("[name='mail']").val()});

        jQuery("#prkt-guest-list-content-form [type='submit']").attr("disabled", "disabled").val("SENDING...");

        jQuery.ajax({
            type: "post",
            url: ajaxurl,
            dataType: 'html',
            data: data,
            async: true,
            success: function (response, textStatus, XMLHttpRequest) {
                var data = jQuery.parseJSON(response);

                jQuery("#prkt-guest-list-content-form form [name='mail']").val("");

                var count = 0;

                jQuery.each(data, function (i, mail) {
                    if (jQuery("div[data-mail='" + mail + "']").length == 0) {
                        jQuery("#prkt-guest-list").append("<div data-mail='" + mail + "' class='mail-invitation'><span class='remove'></span>" + mail + "<span class='state' data-send='no'>Sent</span></div>");
                    }

                    if (jQuery("div[data-mail='" + mail + "']").length > 0 && jQuery("div[data-mail='" + mail + "'] .state").attr("data-send") == "no") {
                        setTimeout(function () {
                            prkt_gl_send_invitation(mail);
                        }, prkt_interval_email_send * count);
                        count++;
                    }
                });

                setTimeout(function () {
                    jQuery("#prkt-guest-list-content-form [type='submit']").removeAttr("disabled").val("SEND INVITATIONS");
                }, prkt_interval_email_send * (count - 1));
                console.log(prkt_interval_email_send * (count - 1));
            },
            error: function (e) {
                alert("An error has occurred.");
            }
        });

        e.preventDefault();
        return false;

    });

    /** DELETE INVITATION
     */

    jQuery(document).on("click", "#prkt-guest-list span.remove", function () {
        var email = jQuery(this).parent().attr("data-mail");

        if (!confirm("Are you sure to delete the invitation to " + email + "?"))
            return;


        var data = {"action": "prkt_gl_ajax_delete_guest", "mail": email};

        jQuery.ajax({
            type: "post",
            url: ajaxurl,
            dataType: 'html',
            data: data,
            async: true,
            success: function (response, textStatus, XMLHttpRequest) {
                var data = jQuery.parseJSON(response);
                if (data) {
                    jQuery("#prkt-guest-list .mail-invitation[data-mail='" + email + "']").fadeOut(function () {
                        jQuery(this).remove()
                    });
                }

            },
            error: function (e) {
                alert("An error has occurred.")
            }
        });
    });

});

/** SEND INVITATION
 */

function prkt_gl_send_invitation(email) {

    var data = {"action": "prkt_gl_ajax_send_invitation", "mail": email};

    jQuery("div[data-mail='" + email + "'] .state").attr("data-send", "in");

    jQuery.ajax({
        type: "post",
        url: ajaxurl,
        dataType: 'html',
        data: data,
        async: true,
        success: function (response, textStatus, XMLHttpRequest) {
            var data = jQuery.parseJSON(response);

            if (data) {
                jQuery("div[data-mail='" + email + "'] .state").attr("data-send", "yes");
            } else {
                jQuery("div[data-mail='" + email + "'] .state").attr("data-send", "no");
                alert("Undelivered email to " + email);
            }
        },
        error: function (e) {
            alert("An error has occurred.")
        }
    });
}
