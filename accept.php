<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once('../../../wp-load.php' );
require_once("prkt-guest-list-invitation.php");

$hash = $_GET["hash"];
$user_id = $_GET["ref"];

if (prkt_gl_accept_invitation($user_id, $hash))
    echo prkt_view("msg-link-accepted");
else
    echo prkt_view("msg-link-invalid");
