<?php
$user_id = get_current_user_id();
$data = get_option("prkt-" . $user_id . "-party-data");
$guests = get_option("prkt-" . $user_id . "-guest-list");
?>

<script src="<?php echo PRFT_URL . "/assets/script.js"; ?>"></script>

<div style="text-align: center;">
  <img class="imagentopmapa" src="/wp-content/uploads/2017/03/imagentopformumapa.jpg" width="1220px" height="auto" style="display: block;margin: auto;max-width: 100%;">
    <img class="imagentopcohete" src="/wp-content/uploads/2017/03/formutopimg.jpg" width="1220px" height="auto" style="display: block;margin: auto;max-width: 100%;">
    <img class="estrellaconpuntos" src="/wp-content/uploads/2017/03/estrellaypuntosright.png">
    <img class="punticosleft" src="/wp-content/uploads/2017/03/punticosleft.png">
</div>

<div id="prkt-guest-list-content-form">
    <form>
        <input type="hidden" name="action" value="prkt_gl_ajax_post_submit_form"/>
        <div class="col-1">
            <label>Party for:</label>
        </div>
        <div class="col-2">
            <label>First name:</label> <input class="form-input" type="text" name="firstname" value="<?php echo (isset($data["firstname"])) ? $data["firstname"] : null; ?>">
        </div>
        <div class="col-2">
            <label>Last name:</label> <input class="form-input" type="text" name="lastname" value="<?php echo (isset($data["lastname"])) ? $data["lastname"] : null; ?>">
        </div>
        <div class="col-2">
            <label>Date:</label> <input class="form-input" type="text" name="date" value="<?php echo (isset($data["date"])) ? $data["date"] : null; ?>">
        </div>
        <div class="col-2">
            <label>Time:</label> <input class="form-input" type="text" name="time" value="<?php echo (isset($data["time"])) ? $data["time"] : null; ?>">
        </div>
        <div class="col-2">
            <label>Rsvp to:</label> <input class="form-input" type="text" name="rsvp" value="<?php echo (isset($data["rsvp"])) ? $data["rsvp"] : null; ?>">
        </div>
        <div class="col-2">
                <label>E-mail</label> <input class="form-input" type="text" name="email" value="<?php echo (isset($data["email"])) ? $data["email"] : null; ?>">
        </div>
        <div class="col-2">
            <label>Phone:</label> <input class="form-input" type="text" name="phone" value="<?php echo (isset($data["phone"])) ? $data["phone"] : null; ?>">
        </div>
        <div class="col-2">
                <label>By:</label> <input class="form-input" type="text" name="dateby" value="<?php echo (isset($data["dateby"])) ? $data["dateby"] : null; ?>">
        </div>


        <div class="col-2 clearl">
            <ul>
        			<li> No Strollers in Playground</li>
        			<li> Adults and Kids Must Have Socks</li>
        			<li> Please fill out waiver online at </br>
                <a href="http://boomkidz.net">www.boomkidz.net</a></li>

            </ul>
        </div>
		    <div class="col-2">
            <p class="textoenteremails">Enter the E-mails of the people you are inviting below. Up to 10 people can be invited at a time, you can always add more E-mails after you have sent the first 10 invitations.</p>
            <label>Email List:</label><br/>

            <ol>
              <li><input class="form-input" type="text" name="correo[]" ></li>
              <li><input class="form-input" type="text" name="correo[]" ></li>
              <li><input class="form-input" type="text" name="correo[]" ></li>
              <li><input class="form-input" type="text" name="correo[]" ></li>
              <li><input class="form-input" type="text" name="correo[]" ></li>
              <li><input class="form-input" type="text" name="correo[]" ></li>
              <li><input class="form-input" type="text" name="correo[]" ></li>
              <li><input class="form-input" type="text" name="correo[]" ></li>
              <li><input class="form-input" type="text" name="correo[]" ></li>
              <li><input class="form-input" type="text" name="correo[]" ></li>
            </ol>




        </div>
        <div class="contenedorboton" style="text-align: center;">
            <input type="submit" class="miboton" value="SEND INVITATIONS">
        </div>
    </form>
</div>

<div id="prkt-guest-list">
    <?php
    /*RECORRE LOS CORREOS Y VERIFICA EL ENVIO*/
	if (is_array($guests) && count($guests) > 0) {
        foreach ($guests as $guest) {
            $state_delivered = ($guest["delivered"]) ? "yes" : "no";
            $state = ($guest["state"]) ? "Accepted" : "Sent";
            echo "<div data-mail='" . $guest["mail"] . "' class='mail-invitation'><span class='remove'></span> " . $guest["mail"] . "<span class='state' data-send='" . $state_delivered . "'>" . $state . "</span></div>";
        }
    }

	/*DISEÑO DIFERENTE PARA LOS USUARIOS*/

    	if( current_user_can('host')){
    		?>
        <style media="screen">
        div#adminmenumain {
          display: none;
        }

        #wpcontent {
          margin-left: 0px!important;
          padding-left: 0px!important;
        }

        .notice {
          display: none;
        }
        html.wp-toolbar {
            padding-top: 0px!important;
        }
        div#wpadminbar {
            display: none;
        }
        </style>
        <?php
    	}


// echo '<pre>',print_r($data,1),'</pre>';
// echo '<pre>',print_r($guests,1),'</pre>';
    ?>
    <img class="juguetebajo" src="/wp-content/uploads/2017/03/juguetes.png">
    <img class="avionbajo" src="/wp-content/uploads/2017/03/avion.png">
</div>
