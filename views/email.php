<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Mail</title>
<link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top" bgcolor="#ffffff" style="background-color:#ffffff;"><br>
    <br>
    <table width="600" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="left" valign="top"><img src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/03/topemail.jpg" width="710" height="240" style="display:block;margin: auto;"></td>
      </tr>
      <tr>
        <td align="center" valign="top" bgcolor="#f47721" style="background-color:#f47721; font-family: 'Roboto Condensed', sans-serif; font-size:30px; color:#ffffff; padding:0px 15px 10px 15px;">

            <!-- CONTENT HERE -->
            <h1 style="font-family: 'Indie Flower', cursive;">You have been invited to <?php echo $firstname; ?> <?php echo $lastname; ?> ’s Birthday!</h1>

			<!--<div>Party for: <?php echo $party_for; ?></div>-->
			<div style="color: white!important;">Date: <?php echo $date; ?></div>
			<div style="color: white!important;">Time: <?php echo $time; ?></div>
			<div style="color: white!important;">Rsvp: <?php echo $rsvp; ?></div>
      <div style="color: white!important;">E-mail: <?php echo $email; ?></div>
			<div style="color: white!important;">Phone: <?php echo $phone; ?></div>
			<div style="color: white!important;padding-bottom: 50px;">By: <?php echo $dateby; ?></div>
      <a href="<?php echo $link; ?>" style="
        display: block;
        background-color: #ffdf43;
        border-radius: 100px;
        color: #2c7bff;
        font-size: 25px;
        font-family: 'Open Sans',sans-serif;
        border-bottom: none!important;
        width: 250px;
        padding: 25px;
        text-decoration: none;
        margin-bottom: 40px;">ACCEPT INVITATION</a>

        </td>
      </tr>
      <tr>
        <td align="left" valign="top"></td>
      </tr>
  </table>
    <br>
    <br></td>
  </tr>
</table>
</body>
</html>
