<?php

/**
 * Plugin Name: Perfekta Guest List Invitation
 * Plugin URI: Perfekta.com
 * Description: Sistema de invitaciones
 * Version: 20170818
 * Author: Perfekta.com
 * Author URI: Perfekta.com
 * License: GPL2
 */
define("PRFT_URL", plugins_url("prkt-guest-list-invitation"));


//Actions
add_action('admin_menu', 'prkt_gl_register_menu_page');
add_action('admin_head', 'prkt_gl_admin_head_page');

//REDIRECT
function redirigir_login() {
    // your code
	$url="/wp-admin/admin.php?page=guest-list-page";
    if ( wp_redirect( $url ) ) {
		exit;
	}

}
add_action('wp_login', 'redirigir_login');
//BACKEND COLOR
add_filter('get_user_option_admin_color','change_admin_color');
function change_admin_color() {
    return 'ectoplasm';
}


//Ajax
add_action('wp_ajax_prkt_gl_ajax_post_submit_form', 'prkt_gl_ajax_post_submit_form');
add_action('wp_ajax_nopriv_prkt_gl_ajax_post_submit_form', 'prkt_gl_ajax_post_submit_form');
add_action('wp_ajax_prkt_gl_ajax_send_invitation', 'prkt_gl_ajax_send_invitation');
add_action('wp_ajax_nopriv_prkt_gl_ajax_send_invitation', 'prkt_gl_ajax_send_invitation');
add_action('wp_ajax_prkt_gl_ajax_delete_guest', 'prkt_gl_ajax_delete_guest');
add_action('wp_ajax_nopriv_prkt_gl_ajax_delete_guest', 'prkt_gl_ajax_delete_guest');

function prkt_gl_register_menu_page() {
    add_menu_page('Guest List', 'Guest List', 'read', 'guest-list-page', 'prkt_gl_menu_page_content', null, 6);
}

function prkt_gl_menu_page_content() {
    echo prkt_view("form");
}

function prkt_gl_admin_head_page() {
    if (!isset($_GET["page"]) || $_GET["page"] != "guest-list-page")
        return;

    echo "<link rel='stylesheet' href='" . PRFT_URL . "/assets/style.css" . "' type='text/css' media='all' />";
}

function prkt_view($filename, $data = array()) {
    extract($data);
    ob_start();
    include(dirname(__FILE__) . "/views/$filename.php");
    return ob_get_clean();
}

function prkt_gl_ajax_post_submit_form() {

    $user_id = get_current_user_id();

    $data = $_POST;

		// $mails=explode(",", $data["mail"]) ;
		// unset($data["mail"]);
		$mails=$data["correo"];
		unset($data["correo"]);



    $guest = get_option("prkt-" . $user_id . "-guest-list");

    if (!is_array($guest))
        $guest = array();

    $list = array();

    foreach ($mails as $mail) {

        if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
            continue;
        }

        $list[] = $mail;

        if (is_null(prkt_gl_get_data_guest($mail)))
            $guest[] = ["mail" => $mail, "state" => false, "hash" => md5($mail), "delivered" => false];
    }

    update_option("prkt-" . $user_id . "-guest-list", $guest);
    update_option("prkt-" . $user_id . "-party-data", $data);

    exit(json_encode($list));
}

/**
 * Recibe una Peticion AJAX para enviar una invitación a traves de un correo electrónico
 */
function prkt_gl_ajax_send_invitation() {

    $data = $_POST;
    $user_id = get_current_user_id();
    $mail = $data["mail"];


    $guest = prkt_gl_get_data_guest($mail);
    $data = array_merge($data, get_option("prkt-" . $user_id . "-party-data"));
    $data["link"] = PRFT_URL . "/accept.php?ref=" . $user_id . "&hash=" . $guest["hash"];

    if ($guest["delivered"])
        exit(json_encode(false));

    $subject = 'Invitation Boomkidz Party';
    $body = prkt_view("email", $data);
    $headers = array('Content-Type: text/html; charset=UTF-8');

    wp_mail($mail, $subject, $body, $headers);

    $guest["delivered"] = true;
    prkt_gl_set_data_guest($mail, $guest);

    exit(json_encode(true));
}

/**
 * Recibe una petición ajax para eliminar un invitado
 */
function prkt_gl_ajax_delete_guest() {
    $email = $_POST["mail"];
    $user_id = get_current_user_id();

    $guests = get_option("prkt-" . $user_id . "-guest-list");
    $data = [];
    foreach ($guests as $guest) {
        if ($guest["mail"] == $email)
            continue;
        $data[] = $guest;
    }

    update_option("prkt-" . $user_id . "-guest-list", $data);

    exit(json_encode(true));
}

/** Obtiene los datos de un invitado dado su email
 *
 * @param type $email
 * @return type
 */
function prkt_gl_get_data_guest($email) {
    $user_id = get_current_user_id();
    $guests = get_option("prkt-" . $user_id . "-guest-list");
    foreach ($guests as $guest) {
        if ($guest["mail"] == $email)
            return $guest;
    }

    return null;
}

/** Guarda la información asociada a un invitado dado por su email
 *
 * @param type $email
 * @param type $data
 * @return type
 */
function prkt_gl_set_data_guest($email, $data, $user_id = null) {
    $user_id = (is_null($user_id)) ? get_current_user_id() : $user_id;
    $guests = get_option("prkt-" . $user_id . "-guest-list");
    $i = 0;
    foreach ($guests as $guest) {
        if ($guest["mail"] == $email) {
            $guests[$i] = $data;
            update_option("prkt-" . $user_id . "-guest-list", $guests);
            return;
        }
        $i++;
    }
}

/** Establece que la invitación de un usuario ha sido acceptada.
 *
 * @param type $user_id
 * @param type $hash
 * @return boolean
 */
function prkt_gl_accept_invitation($user_id, $hash) {
    $guests = get_option("prkt-" . $user_id . "-guest-list");

    if (!is_array($guests))
        return false;

    foreach ($guests as $guest) {
        if ($guest["hash"] == $hash && $guest["state"] == false) {
            $guest["state"] = true;
            prkt_gl_set_data_guest($guest["mail"], $guest, $user_id);
            return true;
        }
    }

    return false;
}

function prkt_gl_add_role_host() {
	remove_role('host');
    add_role('host', 'Party Host', array('read' => true, 'level_1' => true));
}

register_activation_hook(__FILE__, 'prkt_gl_add_role_host');
